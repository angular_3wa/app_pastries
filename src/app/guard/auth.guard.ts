import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { firstValueFrom } from "rxjs";
import { map } from "rxjs";
import { UserService } from "../services/user.service";

@Injectable({
	providedIn: 'root'
})

export class AuthGuard {
	constructor(
		private router: Router,
		private userService: UserService,
	) {}

	async canActivate(): Promise<boolean> {
		try {
			const me: any = await firstValueFrom(this.userService.isLogged().pipe(
				map((res) => {
					if (res) {
						return true
					} else {
						this.router.navigate(['/login']);
						return false;
					}
				})
			));
			return me;
		} catch(error) {
			console.error(error);
			return false;
		}
	}
}