import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appBorderCard]'
})
export class BorderCardDirective {
  
  @Input('appBorderCard') hoverClass:string = '';

  constructor(private el:ElementRef) { }

  @HostListener('mouseenter') onMouseEnter() {
    this.borderCard();
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.borderCard();
  }

  private borderCard() {
    this.el.nativeElement.classList.toggle(this.hoverClass);
  }
}
