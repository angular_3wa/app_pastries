import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment.development';

import Pastrie from '../interfaces/pastrie';
import { Ingredient } from '../interfaces/ingredient';

@Injectable({
  providedIn: 'root',
})
export class PastrieService {
  private _pastries: Pastrie[] = [];

  private _currentPage = new Subject<number>();

  private _httpOptions: object = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    withCredentials: true,
  };

  constructor(private http: HttpClient) {
    this.getPastries().subscribe((res: Pastrie[]) => {
      this._pastries = res;
    });
  }

  getPastries(): Observable<Pastrie[]> {
    return this.http.get<Pastrie[]>(`${environment.apiUrl}/pastries`, this._httpOptions).pipe(
      map((pastries) => pastries.sort((a, b) => b.quantity - a.quantity))
    );
  }

  getTotalPastries(): Observable<number> {
    return this.http.get<number>(`${environment.apiUrl}/pastries-count`, this._httpOptions);
  }

  getPastrie(id: string): Observable<Pastrie> {
    return this.http.get<Pastrie>(`${environment.apiUrl}/pastrie/${id}`, this._httpOptions).pipe(
      map((pastrie) => pastrie)
    );
  }

  getPastrieIngredientsList(id: string): Observable<Ingredient[]> {
    return this.http
      .get<Ingredient[]>(`${environment.apiUrl}/ingredient/${id}`, this._httpOptions)
      .pipe(map((list) => list));
  }

  paginate(first: number, last: number): Observable<Pastrie[]> {
    return this.http.get<Pastrie[]>(
      `${environment.apiUrl}/pastries/order-quantity/${first}/${last}`,
      this._httpOptions
    );
  }

  search(word: string): Observable<Pastrie[]> {
    return this.http.get<Pastrie[]>(`${environment.apiUrl}/pastries-search/${word}`, this._httpOptions);
  }

  update(id: string, pastrie: any): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}/pastrie/${id}`, pastrie, this._httpOptions);
  }

  create(pastrie: any): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}/pastrie`, pastrie.value, this._httpOptions);
  }

  delete(id: string): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}/delete/${id}`, this._httpOptions);
  }

  get pastries(): Pastrie[] {
    return this._pastries;
  }

  getCurrentPage(): Subject<number> {
    return this._currentPage;
  }

  setCurrentPage(page: number): void {
    this._currentPage.next(page);
  }
}
