import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable, catchError, retry, throwError } from 'rxjs';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _httpOptions: object = {
    withCredentials: true,
  };


  constructor(
    private http: HttpClient
  ) { }

  login(email: string, password: string): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}/login`, {email, password}, this._httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  };

  logout(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/logout`, this._httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      )
  }

  createUser(user: FormGroup) {
    return this.http.post<any>(`${environment.apiUrl}/register`, user.value, this._httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      console.error('An error occurred:', error.error);
    } else {
      console.error(`Backend returned code ${error.status}, body was: ${error.message}`);
    }
    return throwError(() => new Error('Something bad happened, please try again later...'));
  }
}
