import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userName: EventEmitter<string | null> = new EventEmitter();
  private _httpOptions: object = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    withCredentials: true
  };
  constructor(private http: HttpClient) { }

  setUserName(userName: string) {
    localStorage.setItem('user', userName);
    return this.userName.emit(userName);
  }

  removeUserName() {
    localStorage.removeItem('user');
    return this.userName.emit(null);
  }

  isLogged(): Observable<boolean> {
    return this.me()
      .pipe(
        map((res) => res),
        catchError(() => of(false)),
      )
  }

  me(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/me`, this._httpOptions)
      .pipe(
        map((res) => {
          this.userName.emit(res.name);
          return res;
        })
      );
  }
}
