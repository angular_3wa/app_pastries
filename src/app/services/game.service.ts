import { Injectable } from '@angular/core';
import Pastrie from '../interfaces/pastrie';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  selectedPastries: Pastrie[] = [];

  constructor() { }

  setSelectedPastries(pastries: Pastrie[]): void{
    this.selectedPastries = pastries;
  };

  getSelectedPastries(): Pastrie[]{
    return this.selectedPastries;
  }
}
