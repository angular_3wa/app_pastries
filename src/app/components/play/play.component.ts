import { Component } from '@angular/core';
import { GameService } from '../../services/game.service';
import Pastrie from 'src/app/interfaces/pastrie';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss']
})
export class PlayComponent {
  canPlay: boolean = false;
  selectedPastries: Pastrie[] = [];
  isLogged: boolean = false;

  constructor(
    private gameService: GameService,
    private userService: UserService,
  ) {
    this.canPlay = this.gameService.getSelectedPastries().length >= 3 ? true : false;
    this.selectedPastries = this.gameService.getSelectedPastries();
    this.userService.userName.subscribe((res) => console.log(res))
  }
}
