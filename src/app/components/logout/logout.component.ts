import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent {
  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
  ) {
    this.authService.logout().subscribe(() => {
      this.userService.removeUserName();
      this.router.navigateByUrl('/');
    });
    
  }
}
