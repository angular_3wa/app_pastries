import { Component } from '@angular/core';
import { PastrieService } from '../../services/pastrie.service';
import Pastrie from '../../interfaces/pastrie';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  pastries: Pastrie[] = [];
  addPastrie: boolean = false;
  modifyId: string | undefined;
  deleteId: string[] = [];

  constructor(
    private pastrieService: PastrieService,
  ) {
    this.loadInitialPastries();
  }

  loadInitialPastries(): void {
    this.pastrieService.getPastries().subscribe((res: Pastrie[]) => {
      this.pastries = res;
    });
  }

  setSearchPastrie(pastries: Pastrie[]): void {
    this.pastries = pastries;
  }

  modifyPastrie(pastrieId: string): void {
    this.modifyId = this.modifyId === pastrieId ? undefined : pastrieId;
    this.addPastrie = false;
    this.deleteId = [];
  }

  createPastrie(): void {
    this.addPastrie = true;
    this.modifyId = undefined;
    this.deleteId = [];
  }

  deletePastrie(pastrieId: string, pastrieName: string) {
    this.deleteId = this.deleteId[0] === pastrieId ? [] : [pastrieId, pastrieName];
    this.modifyId = undefined;
    this.addPastrie = false;
  }

  abort($event: boolean): void {
    this.modifyId = undefined;
    this.addPastrie = false;
    this.deleteId = [];
  }

  catchPastrie($event: Pastrie): void {
    this.pastries.push($event);
  }

  updatePastrie($event: string): void{
    console.log($event);
    this.loadInitialPastries();
  }
}
