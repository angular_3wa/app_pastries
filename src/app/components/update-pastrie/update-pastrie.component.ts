import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { PastrieService } from '../../services/pastrie.service';
import Pastrie from '../../interfaces/pastrie';
import { Ingredient } from '../../interfaces/ingredient';

@Component({
  selector: 'app-update-pastrie',
  templateUrl: './update-pastrie.component.html',
  styleUrls: ['./update-pastrie.component.scss'],
})
export class UpdatePastrieComponent implements OnChanges {
  @Input() modifyId: string | undefined;
  pastrie: Pastrie | undefined;
  pastrieForm = this.fb.group({
    ref: ['', [Validators.required, Validators.minLength(5)]],
    name: ['', [Validators.required, Validators.minLength(5)]],
    description: ['', [Validators.required, Validators.minLength(30)]],
    url: ['', [Validators.required]],
    quantity: [0, [Validators.required, Validators.min(0)]],
    order: [0, [Validators.required, Validators.min(0)]],
    tags: [[''], [Validators.required, Validators.pattern('^[\\wÀ-ÿ]+(?:,[\\wÀ-ÿ\\s]+)*$')]],
    like: ['', [Validators.required]]
  });

  @Output() aborting: EventEmitter<boolean> = new EventEmitter();
  @Output() update: EventEmitter<string> = new EventEmitter();

  constructor(
    private pastrieService: PastrieService,
    private fb: FormBuilder
  ) {}

  get ref() {
    return this.pastrieForm.get('ref');
  }

  get name() {
    return this.pastrieForm.get('name');
  }

  get description() {
    return this.pastrieForm.get('description');
  }

  get url() {
    return this.pastrieForm.get('url');
  }

  get quantity() {
    return this.pastrieForm.get('quantity');
  }

  get order() {
    return this.pastrieForm.get('order');
  }

  get tags() {
    return this.pastrieForm.get('tags');
  }

  get like() {
    return this.pastrieForm.get('like');
  }

  get ingredients() {
    return this.pastrieForm.get('ingredients');
  }

  onSubmit(id: string) {
    this.pastrieService.update(id, this.pastrieForm.value).subscribe();
    this.update.emit("update now!");
  }

  abort() {
    this.aborting.emit(true);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.modifyId !== undefined) {
      this.pastrieService.getPastrie(this.modifyId).subscribe((pastrie: Pastrie) => {
        this.pastrieForm.patchValue({
          ref: pastrie.ref,
          name: pastrie.name,
          description: pastrie.description,
          url: pastrie.url,
          quantity: pastrie.quantity,
          order: pastrie.order,
          tags: pastrie.tags,
          like: pastrie.like
        });
      });
    }
  }
}
