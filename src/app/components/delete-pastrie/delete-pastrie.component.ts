import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PastrieService } from '../../services/pastrie.service';

@Component({
  selector: 'app-delete-pastrie',
  templateUrl: './delete-pastrie.component.html',
  styleUrls: ['./delete-pastrie.component.scss']
})
export class DeletePastrieComponent {
  @Input() deleteId: string[] = [];

  @Output() aborting: EventEmitter<boolean> = new EventEmitter();
  @Output() update: EventEmitter<string> = new EventEmitter();

  constructor(private pastrieService: PastrieService) {}

  delete(pastrieId: string) {
    this.pastrieService
      .delete(pastrieId)
      .subscribe();
    this.update.emit("update now!");
    this.abort();
  }
  abort() {
    this.aborting.emit(true);
  }

}
