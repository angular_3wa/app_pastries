import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as bcrypt from 'bcryptjs';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  userForm: FormGroup;
  successMessage: boolean = false;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService
  ) {
    this.userForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(5)]],
      email: ['', [Validators.required, Validators.email]],
      address: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.pattern('^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*[!@#$%^&*()_\\-+=?]).{8,}$')]],
      password_verify: ['', [Validators.required]]
    }, {
      validators: this.passwordMatchValidator
    });
  }

  get name() {
    return this.userForm.get('name');
  }

  get email() {
    return this.userForm.get('email');
  }

  get address() {
    return this.userForm.get('address');
  }

  get password() {
    return this.userForm.get('password');
  }

  get password_verify() {
    return this.userForm.get('password_verify');
  }

  passwordMatchValidator(formGroup: FormGroup) {
    const password = formGroup.get('password')?.value;
    const password_verify = formGroup.get('password_verify')?.value;

    if (password !== password_verify) {
      formGroup.get('password_verify')?.setErrors({ mismatch: true });
    }
  }

  onSubmit() {
    this.authService.createUser(this.userForm).subscribe(res => {
      if (res) {
        this.successMessage = true;
      }
    });
  }
}
