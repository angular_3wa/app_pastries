import { Component, Input, Output, EventEmitter } from '@angular/core';
import { PastrieService } from '../../services/pastrie.service';
import Paginate from '../../interfaces/paginate';

@Component({
  selector: 'app-paginate',
  templateUrl: './paginate.component.html',
  styleUrls: ['./paginate.component.scss'],
})
export class PaginateComponent {
  @Input() parentPreference: string[] = [];

  currentPage = 0;
  totalPastries = 0;
  pastriesPerPage = 3;
  hoverClass = 'shadow';
  pages: number[] = [];

  @Output() pagin = new EventEmitter<Paginate>();

  constructor(private pastrieService: PastrieService) {
    this.initializeTotalPastries();
    this.initializeCurrentPage();
  }

  initializeTotalPastries(): void {
    this.pastrieService.getTotalPastries().subscribe((res) => {
      this.totalPastries = res;
      this.pages = [...Array(Math.ceil(this.totalPastries / this.pastriesPerPage)).keys()];
    });
  }

  initializeCurrentPage(): void {
    this.pastrieService.getCurrentPage().subscribe((page) => {
      this.currentPage = page;
    });
  }

  calculPagin(page: number): void {
    this.currentPage = page;
    this.pastrieService.setCurrentPage(page);
    const start = page * this.pastriesPerPage;
    const end = start + this.pastriesPerPage;
    const obj: Paginate = { start, end };
    this.pagin.emit(obj);
  }
}
