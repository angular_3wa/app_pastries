import { Component, EventEmitter, Output } from '@angular/core';
import { PastrieService } from '../../services/pastrie.service';
import Pastrie from '../../interfaces/pastrie';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {

  word: string = '';
  @Output() searchPastries: EventEmitter<Pastrie[]> = new EventEmitter();
  @Output() dashboardSearch: EventEmitter<Pastrie[]> = new EventEmitter();

  constructor(private pastrieService: PastrieService) { }

  onChangeEmit(w: string) {
    if (w.length > 0) {
      this.pastrieService.search(w).subscribe((res: Pastrie[]) => {
        this.searchPastries.emit(res);
        this.dashboardSearch.emit(res);
      });
    } else {
      this.onReload();
    }
  }

  onReload() {
    this.word = '';
    this.pastrieService.paginate(0, 3).subscribe((res: Pastrie[]) => {
      this.searchPastries.emit(res);
    });
    this.pastrieService.getPastries().subscribe((res: Pastrie[]) => {
      this.dashboardSearch.emit(res);
    })
  }
}
