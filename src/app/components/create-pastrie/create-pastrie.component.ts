import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { PastrieService } from '../../services/pastrie.service';
import Pastrie from '../../interfaces/pastrie';

@Component({
  selector: 'app-create-pastrie',
  templateUrl: './create-pastrie.component.html',
  styleUrls: ['./create-pastrie.component.scss']
})
export class CreatePastrieComponent {
  @Input() addPastrie = false;
  pastrieForm = this.fb.group({
    ref: ['', [Validators.required, Validators.minLength(5)]],
    name: ['', [Validators.required, Validators.minLength(5)]],
    description: ['', [Validators.required, Validators.minLength(30)]],
    url: ['', [Validators.required]],
    quantity: ['', [Validators.required]],
    order: ['', [Validators.required]],
    tags: ['', [Validators.required,  Validators.pattern('^[\\wÀ-ÿ]+(?:,[\\wÀ-ÿ\\s]+)*$')]],
    like: ['', [Validators.required]]
  });

  @Output() aborting = new EventEmitter<boolean>();
  @Output() newPastrie = new EventEmitter<Pastrie>();

  constructor(
    private pastrieService: PastrieService,
    private fb: FormBuilder
  ) {}

  get ref() {
    return this.pastrieForm.get('ref');
  }

  get name() {
    return this.pastrieForm.get('name');
  }

  get description() {
    return this.pastrieForm.get('description');
  }

  get url() {
    return this.pastrieForm.get('url');
  }

  get quantity() {
    return this.pastrieForm.get('quantity');
  }

  get order() {
    return this.pastrieForm.get('order');
  }

  get tags() {
    return this.pastrieForm.get('tags');
  }

  get like() {
    return this.pastrieForm.get('like');
  }

  get ingredients() {
    return this.pastrieForm.get('ingredients');
  }

  onSubmit() {
    this.pastrieService.create(this.pastrieForm).subscribe((res: Pastrie) => {
      this.newPastrie.emit(res);
    });
  }

  abort() {
    this.aborting.emit(true);
  }
}
