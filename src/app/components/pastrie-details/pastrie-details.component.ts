import { Component, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { PastrieService } from '../../services/pastrie.service';
import Pastrie from '../../interfaces/pastrie';
import { Ingredient } from '../../interfaces/ingredient';
import { GameService } from '../../services/game.service';

@Component({
  selector: 'app-pastrie-details',
  templateUrl: './pastrie-details.component.html',
  styleUrls: ['./pastrie-details.component.scss'],
})
export class PastrieDetailsComponent implements OnChanges {
  @Input() selectId?: string;

  selectedPastrie: Pastrie | undefined;
  ingredients: Ingredient[] = [];
  selected: string[] = [];

  @Output() changePreference = new EventEmitter<string>();

  constructor(private pastrieService: PastrieService, private gameService: GameService) {
    this.selected = this.gameService.getSelectedPastries().map(elem => elem.id);
  }

  preference(id: string): void {
    this.changePreference.emit(id);
    if (this.selected.includes(id)) {
      this.selected.splice(this.selected.indexOf(id), 1);
    } else {
      this.selected.push(id);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.selectId !== undefined) {
      this.pastrieService.getPastrie(this.selectId).subscribe(pastrie => {
        this.selectedPastrie = pastrie;
      });
      this.pastrieService.getPastrieIngredientsList(this.selectId).subscribe(ingredients => {
        this.ingredients = [...ingredients];
      });
    } else {
      this.selectedPastrie = undefined;
      this.ingredients = [];
    }
  }
}
