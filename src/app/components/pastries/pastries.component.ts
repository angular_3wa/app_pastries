import { Component, Input } from '@angular/core';
import { PastrieService } from '../../services/pastrie.service';
import Pastrie from '../../interfaces/pastrie';
import Paginate from '../../interfaces/paginate';
import { GameService } from '../../services/game.service';

@Component({
  selector: 'app-pastries',
  templateUrl: './pastries.component.html',
  styleUrls: ['./pastries.component.scss'],
})
export class PastriesComponent {
  @Input() pastries: Pastrie[] = [];

  titlePage: string = 'Liste des pâtisseries à gagner';
  selectId: string | undefined;
  parentPreference: string[] = [];
  hoverClass: string = 'shadow';

  constructor(
    private pastrieService: PastrieService,
    private gameService: GameService
  ) {
    this.loadInitialPastries();
    this.loadInitialParentPreference();
  }

  loadInitialPastries(): void {
    this.pastrieService.paginate(0, 3).subscribe((res: Pastrie[]) => {
      this.pastries = res;
    });
  }

  loadInitialParentPreference(): void {
    this.parentPreference = this.gameService.getSelectedPastries().map((elem) => elem.id);
  }

  setSearchPastrie(pastries: Pastrie[]): void {
    this.pastries = pastries;
  }

  select(pastrieId: string): void {
    this.selectId = this.selectId === pastrieId ? undefined : pastrieId;
  }

  changeParentPreference(pastrieId: string): void {
    const index = this.parentPreference.indexOf(pastrieId);
    if (index !== -1) {
      this.parentPreference.splice(index, 1);
    } else {
      this.parentPreference.push(pastrieId);
    }

    const selectedPastries: Pastrie[] = this.pastries.filter((pastrie) =>
      this.parentPreference.includes(pastrie.id)
    );
    this.gameService.setSelectedPastries(selectedPastries);
  }

  getPagination(paginate: Paginate): void {
    const { start, end } = paginate;
    this.pastrieService.paginate(start, end).subscribe((res: Pastrie[]) => {
      this.pastries = res;
    });
  }

  onSelect(pastrieId: string): void {
    this.selectId = this.selectId === pastrieId ? undefined : pastrieId;
  }
}








