import { Component } from '@angular/core';
import { ClockService } from '../../services/clock.service';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.scss']
})
export class ClockComponent {
  time : String|undefined;
  constructor(private clockService : ClockService) {
    this.clockService.time.subscribe((now : Date) => {
      let hours : string = now.getHours().toString().padStart(2, '0');
      let minutes : string = now.getMinutes().toString().padStart(2, '0');
      let seconds : string = now.getSeconds().toString().padStart(2, '0');
      this.time = `${hours}:${minutes}:${seconds}`;
    })
  }
}