import { AfterViewInit, Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  private _title: string = 'app-yams';
  private _userName: string | null = null;

  constructor(
    private userService: UserService,
  ) {}

  get title(): string {
    return this._title;
  }

  get userName(): string | null {
    return this._userName;
  }

  set userName(userName: string | null) {
    this._userName = userName;
  }

  ngOnInit(): void {
    this.userService.userName.subscribe((res) => {
      this.userName = res;
    });
    if (this.userName === null) this.userName = localStorage.getItem('user');
  }

}
