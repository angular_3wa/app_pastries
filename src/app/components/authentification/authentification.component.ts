import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import User from '../../interfaces/user';

@Component({
  selector: 'app-authentification',
  templateUrl: './authentification.component.html',
  styleUrls: ['./authentification.component.scss']
})
export class AuthentificationComponent {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
  ) {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', [Validators.required, Validators.pattern('^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*[!@#$%^&*()_\\-+=?]).{8,}$')]]
    })
  };

  get email() {
    return this.form.get('email');
  };

  get password() {
    return this.form.get('password')
  }

  log() {
    const val = this.form.value;
    if (val.email && val.password) {
      this.authService
        .login(val.email, val.password)
        .subscribe(() => {
          this.userService.me().subscribe((res) => this.userService.setUserName(res.name));
        });
      setTimeout(() => {this.router.navigateByUrl('/');}, 500);
    }
  }
}
