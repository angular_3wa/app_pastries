import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { PastriesComponent } from './components/pastries/pastries.component';
import { PastrieDetailsComponent } from './components/pastrie-details/pastrie-details.component';
import { BorderCardDirective } from './border-card.directive';
import { ClockComponent } from './components/clock/clock.component';
import { SearchComponent } from './components/search/search.component';
import { PaginateComponent } from './components/paginate/paginate.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './components/register/register.component';
import { UpdatePastrieComponent } from './components/update-pastrie/update-pastrie.component';
import { CreatePastrieComponent } from './components/create-pastrie/create-pastrie.component';
import { DeletePastrieComponent } from './components/delete-pastrie/delete-pastrie.component';
import { AuthentificationComponent } from './components/authentification/authentification.component';
import { PlayComponent } from './components/play/play.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AuthGuard } from './guard/auth.guard';
import { LogoutComponent } from './components/logout/logout.component';
import { NavbarComponent } from './components/navbar/navbar.component';



const routes: Routes = [
    { path: '', component: PastriesComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'play', component: PlayComponent },
    { path: 'login', component: AuthentificationComponent },
    { path: 'logout', component: LogoutComponent, canActivate: [AuthGuard] },
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] }
]

@NgModule({
  declarations: [
    AppComponent,
    PastriesComponent,
    PastrieDetailsComponent,
    BorderCardDirective,
    ClockComponent,
    SearchComponent,
    PaginateComponent,
    RegisterComponent,
    UpdatePastrieComponent,
    CreatePastrieComponent,
    DeletePastrieComponent,
    AuthentificationComponent,
    PlayComponent,
    DashboardComponent,
    LogoutComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
