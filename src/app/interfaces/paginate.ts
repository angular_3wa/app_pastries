export default interface Paginate {
	start : number;
	end : number;
}